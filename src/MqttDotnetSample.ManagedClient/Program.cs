﻿// Setup and start a managed MQTT client.
using System.Text;

using MQTTnet;
using MQTTnet.Client.Options;
using MQTTnet.Extensions.ManagedClient;

var options = new ManagedMqttClientOptionsBuilder()
    .WithAutoReconnectDelay(TimeSpan.FromSeconds(5))
    .WithClientOptions(new MqttClientOptionsBuilder()
        .WithTcpServer("localhost", 5000)
        .Build())
    .Build();

var mqttClient = new MqttFactory().CreateManagedMqttClient();
await mqttClient.SubscribeAsync(new MqttTopicFilterBuilder().WithTopic("my/topic").Build());
mqttClient.UseApplicationMessageReceivedHandler(builder =>
{
    var message = builder.ApplicationMessage;
    Console.WriteLine("topic: " + message.Topic);
    Console.WriteLine(Encoding.UTF8.GetString(message.Payload));
});

await mqttClient.StartAsync(options);
await mqttClient.PublishAsync(builder => builder
    .WithTopic("my/topic")
    .WithPayload(Encoding.UTF8.GetBytes("hello managed client.")));

// StartAsync returns immediately, as it starts a new thread using Task.Run,
// and so the calling thread needs to wait.
Console.WriteLine("subscribed...");
Console.ReadLine();

await mqttClient.StopAsync();
