using MQTTnet;
using MQTTnet.Server;

namespace MqttDotnetSample.Server;

public class Worker : BackgroundService
{
    private readonly ILogger<Worker> _logger;

    public Worker(ILogger<Worker> logger)
    {
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var options = new MqttServerOptionsBuilder()
            .WithDefaultEndpointPort(5000)
            .Build();

        var mqttServer = new MqttFactory().CreateMqttServer();

        await mqttServer.StartAsync(options);

        var tcs = new TaskCompletionSource();
        stoppingToken.Register(() => tcs.SetCanceled());

        await tcs.Task;

        await mqttServer.StopAsync();
    }
}
